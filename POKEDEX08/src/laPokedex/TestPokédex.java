package laPokedex;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.LinkedHashMap;

class FiltroDeLaPokedexTest {
	
	

    @Test
    void testCasoDePrueba1() {
        String input = "30\nCHARIZARD\nFIRE/FLYING - KANTO\nFURRET\nNORMAL - JOHTO\nRELICANTH\nWATER/ROCK - HOENN\nINFERNAPE\nFIRE/FIGHTING - SINNOH\nEELEKTROSS\nELECTRIC - UNOVA\nGRENINJA\nWATER/DARK - KALOS\nINCINEROAR\nFIRE/DARK - ALOLA\nSOBBLE\nWATER - GALAR\nKLEAVOR\nROCK/BUG - HISUI\nFLORAGATO\nGRASS - PALDEA\nBULBASAUR\nGRASS/POISON - KANTO\nHERACROSS\nBUG/FIGHTING - JOHTO\nMETAGROSS\nSTEEL/PSYCHIC - HOENN\nTOGEKISS\nFAIRY/FLYING - SINNOH\nZEKROM\nDRAGON/ELECTRIC - UNOVA\nKLEFKI\nSTEEL/FAIRY - KALOS\nDECIDUEYE\nGRASS/GHOST - ALOLA\nSNOM\nICE/BUG - GALAR\nURSALUNA\nNORMAL/GROUND - HISUI\nCLODSIRE\nPOISON/GROUND - PALDEA\nDRAGONITE\nDRAGON/FLYING - KANTO\nTYRANITAR\nROCK/DARK - JOHTO\nMEDICHAM\nFIGHTING/PSYCHIC - HOENN\nMAMOSWINE\nICE/GROUND - SINNOH\nBEARTIC\nICE - UNOVA\nAEGISLASH\nSTEEL/GHOST - KALOS\nMIMIKYU\nGHOST/FAIRY - ALOLA\nETERNATUS\nDRAGON/POISON - GALAR\nWYRDEER\nNORMAL/PSYCHIC - HISUI\nPAWMI\nELECTRIC - PALDEA\nCHARIZARD\nGRENINJA\nINFERNAPE\nJOHTO\nPALDEA\nFIRE\nWATER\nNEVER GONNA GIVE YOU UP\nBUEN DATO\n";

        InputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);

        FiltroDeLaPokedex.main(new String[]{});

        assertEquals("CHARIZARD entrydex:" + 
        			 "FIRE/FLYING - KANTO\n" +
                     "GRENINJA entrydex: WATER/DARK - KALOS\n" +
                     "INFERNAPE entrydex: FIRE/FIGHTING - SINNOH\n" +
                     "Pokemon from the JOHTO region:\n" +
                     "FURRET: NORMAL - JOHTO\n" +
                     "HERACROSS: BUG/FIGHTING - JOHTO\n" +
                     "TYRANITAR: ROCK/DARK - JOHTO\n" +
                     "Pokemon from the PALDEA region:\n" +
                     "FLORAGATO: GRASS - PALDEA\n" +
                     "CLODSIRE: POISON/GROUND - PALDEA\n" +
                     "PAWMI: ELECTRIC - PALDEA\n" +
                     "FIRE type Pokemon:\n" +
                     "CHARIZARD: FIRE/FLYING - KANTO\n" +
                     "INFERNAPE: FIRE/FIGHTING - SINNOH\n" +
                     "INCINEROAR: FIRE/DARK - ALOLA\n" +
                     "WATER type Pokemon:\n" +
                     "RELICANTH: WATER/ROCK - HOENN\n" +
                     "GRENINJA: WATER/DARK - KALOS\n" +
                     "SOBBLE: WATER - GALAR\n" +
                     "Vayanse a Digimon, inutiles\n" +
                     "Gracias por usar la Pokedex. Tu total de búsquedas ha sido de 8, de las cuales 1 han sido inútiles por tu parte.\n",
                     getSystemOut());
    }

    @Test
    void testCasoDePrueba2() {
        String input = "3\nDARKRAI\nDARK - SINNOH\nABSOL\nDARK - HOENN\nPIKACHU\nELECTRIC - KANTO\nELECTRIC\nPIKACHU\nNEVERGONNAGIVEYOUUP\nDARK\nBUEN DATO\n";

        InputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);

        FiltroDeLaPokedex.main(new String[]{});

        assertEquals("ELECTRIC type Pokemon:\n" +
                     "PIKACHU: ELECTRIC - KANTO\n" +
                     "PIKACHU entrydex: ELECTRIC - KANTO\n" +
                     "Vayanse a Digimon, inutiles\n" +
                     "DARK type Pokemon:\n" +
                     "DARKRAI: DARK - SINNOH\n" +
                     "ABSOL: DARK - HOENN\n" +
                     "Gracias por usar la Pokedex. Tu total de búsquedas ha sido de 4, de las cuales 1 han sido inútiles por tu parte.\n",
                     getSystemOut());
    }
    @Test
    void testCasoDePrueba3() {
        String input = "3\nREGICE\nICE - HOENN\nJELLICENT\nWATER/GHOST - UNOVA\nPIKACHU\nELECTRIC - KANTO\nREGICE\nJELLICENT\nNEVERGONNAGIVEYOUUP\nICE\nBUEN DATO\n";

        InputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);

        FiltroDeLaPokedex.main(new String[]{});

        assertEquals("REGICE entrydex: ICE - HOENN\n" +
                     "JELLICENT entrydex: WATER/GHOST - UNOVA\n" +
                     "Vayanse a Digimon, inutiles\n" +
                     "ICE type Pokemon:\n" +
                     "REGICE: ICE - HOENN\n" +
                     "Gracias por usar la Pokedex. Tu total de búsquedas ha sido de 4, de las cuales 1 han sido inútiles por tu parte.\n",
                     getSystemOut());
    }
    @Test
    void testCasoDePrueba4() {
        String input = "3\nSCATTERBUG\nBUG - KALOS\nTOGEDEMARU\nSTEEL/ELECTRIC - ALOLA\nSCORBUNNY\nFIRE - GALAR\nGALAR\nSCATTERBUG\nALOLA\nBUG\nNEVERGONNAGIVEYOUUP\nBUEN DATO\n";

        InputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);

        FiltroDeLaPokedex.main(new String[]{});

        assertEquals("Pokemon from the GALAR region:\n" +
                     "SCORBUNNY: FIRE - GALAR\n" +
                     "SCATTERBUG entrydex: BUG - KALOS\n" +
                     "Pokemon from the ALOLA region:\n" +
                     "TOGEDEMARU: STEEL/ELECTRIC - ALOLA\n" +
                     "BUG type Pokemon:\n" +
                     "SCATTERBUG: BUG - KALOS\n" +
                     "Vayanse a Digimon, inutiles\n" +
                     "Gracias por usar la Pokedex. Tu total de búsquedas ha sido de 5, de las cuales 1 han sido inútiles por tu parte.\n",
                     getSystemOut());
    }
    @Test
    void testCasoDePrueba5() {
        String input = "3\nFROAKIE\nWATER - KALOS\nCHARJABUG\nBUG/ELECTRIC - ALOLA\nZACIAN\nSTEEL/FAIRY - GALAR\nBUG\nELECTRIC\nWATER\nKALOS\nFROAKIE\nCHARJABUG\nGALAR\nNEVERGONNAGIVEYOUUP\nBUEN DATO\n";

        InputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);

        FiltroDeLaPokedex.main(new String[]{});

        assertEquals("BUG type Pokemon:\n" +
                     "CHARJABUG: BUG/ELECTRIC - ALOLA\n" +
                     "ELECTRIC type Pokemon:\n" +
                     "CHARJABUG: BUG/ELECTRIC - ALOLA\n" +
                     "WATER type Pokemon:\n" +
                     "FROAKIE: WATER - KALOS\n" +
                     "Pokemon from the KALOS region:\n" +
                     "FROAKIE: WATER - KALOS\n" +
                     "FROAKIE entrydex: WATER - KALOS\n" +
                     "CHARJABUG entrydex: BUG/ELECTRIC - ALOLA\n" +
                     "Pokemon from the GALAR region:\n" +
                     "ZACIAN: STEEL/FAIRY - GALAR\n" +
                     "Vayanse a Digimon, inutiles\n" +
                     "Gracias por usar la Pokedex. Tu total de búsquedas ha sido de 8, de las cuales 1 han sido inútiles por tu parte.\n",
                     getSystemOut());
    }
    @Test
    void testCasoDePrueba6() {
        String input = "3\nTORCHIC\nFIRE - HOENN\nGARDEVOIR\nPSYCHIC/FAIRY - HOENN\nBLIPBUG\nBUG - GALAR\nBUG\nGARDEVOIR\nHOENN\nFIRE\nPSYCHIC\nGALAR\nBLIPBUG\nBUG\nNEVERGONNAGIVEYOUUP\nBUEN DATO\n";

        InputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);

        FiltroDeLaPokedex.main(new String[]{});

        assertEquals("BUG type Pokemon:\n" +
                     "BLIPBUG: BUG - GALAR\n" +
                     "GARDEVOIR entrydex: PSYCHIC/FAIRY - HOENN\n" +
                     "Pokemon from the HOENN region:\n" +
                     "TORCHIC: FIRE - HOENN\n" +
                     "GARDEVOIR: PSYCHIC/FAIRY - HOENN\n" +
                     "FIRE type Pokemon:\n" +
                     "TORCHIC: FIRE - HOENN\n" +
                     "PSYCHIC type Pokemon:\n" +
                     "GARDEVOIR: PSYCHIC/FAIRY - HOENN\n" +
                     "Pokemon from the GALAR region:\n" +
                     "BLIPBUG: BUG - GALAR\n" +
                     "BLIPBUG entrydex: BUG - GALAR\n" +
                     "BUG type Pokemon:\n" +
                     "BLIPBUG: BUG - GALAR\n" +
                     "Vayanse a Digimon, inutiles\n" +
                     "Gracias por usar la Pokedex. Tu total de búsquedas ha sido de 9, de las cuales 1 han sido inútiles por tu parte.\n",
                     getSystemOut());
    }
    @Test
    void testCasoDePrueba7() {
        String input = "3\nVOLTORB\nELECTRIC - KANTO\nCLEFAIRY\nFAIRY - KANTO\nZACIAN\nSTEEL/FAIRY - GALAR\nELECTRIC\nKANTO\nGALAR\nFAIRY\nCLEFAIRY\nZACIAN\nNEVERGONNAGIVEYOUUP\nBUEN DATO\n";

        InputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);

        FiltroDeLaPokedex.main(new String[]{});

        assertEquals("ELECTRIC type Pokemon:\n" +
                     "VOLTORB: ELECTRIC - KANTO\n" +
                     "Pokemon from the KANTO region:\n" +
                     "VOLTORB: ELECTRIC - KANTO\n" +
                     "CLEFAIRY: FAIRY - KANTO\n" +
                     "Pokemon from the GALAR region:\n" +
                     "ZACIAN: STEEL/FAIRY - GALAR\n" +
                     "FAIRY type Pokemon:\n" +
                     "CLEFAIRY: FAIRY - KANTO\n" +
                     "ZACIAN: STEEL/FAIRY - GALAR\n" +
                     "CLEFAIRY entrydex: FAIRY - KANTO\n" +
                     "ZACIAN entrydex: STEEL/FAIRY - GALAR\n" +
                     "Vayanse a Digimon, inutiles\n" +
                     "Gracias por usar la Pokedex. Tu total de búsquedas ha sido de 7, de las cuales 1 han sido inútiles por tu parte.\n",
                     getSystemOut());
    }
    @Test
    void testCasoDePrueba8() {
        String input = "3\nLUCARIO\nSTEEL/FIGHTING - SINNOH\nSTEELIX\nGROUND/STEEL - JOHTO\nHERACROSS\nBUG/FIGHTING - JOHTO\nFIGHTING\nJOHTO\nLUCARIO\nSINNOH\nSTEELIX\nSTEEL\nNEVERGONNAGIVEYOUUP\nBUEN DATO\n";

        InputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);

        FiltroDeLaPokedex.main(new String[]{});

        assertEquals("FIGHTING type Pokemon:\n" +
                     "LUCARIO: STEEL/FIGHTING - SINNOH\n" +
                     "HERACROSS: BUG/FIGHTING - JOHTO\n" +
                     "Pokemon from the JOHTO region:\n" +
                     "STEELIX: GROUND/STEEL - JOHTO\n" +
                     "HERACROSS: BUG/FIGHTING - JOHTO\n" +
                     "LUCARIO entrydex: STEEL/FIGHTING - SINNOH\n" +
                     "Pokemon from the SINNOH region:\n" +
                     "LUCARIO: STEEL/FIGHTING - SINNOH\n" +
                     "STEELIX entrydex: GROUND/STEEL - JOHTO\n" +
                     "STEEL type Pokemon:\n" +
                     "LUCARIO: STEEL/FIGHTING - SINNOH\n" +
                     "STEELIX: GROUND/STEEL - JOHTO\n" +
                     "Vayanse a Digimon, inutiles\n" +
                     "Gracias por usar la Pokedex. Tu total de búsquedas ha sido de 7, de las cuales 1 han sido inútiles por tu parte.\n",
                     getSystemOut());
    }
    @Test
    void testCasoDePrueba9() {
        String input = "3\nENAMORUS\nFAIRY/FLYING - HISUI\nAMPHAROS\nELECTRIC - JOHTO\nREGISTEEL\nSTEEL - HOENN\nHOENN\nFLYING\nGALAR\nFIRE\nREGISTEEL\nSTEEL\nNEVERGONNAGIVEYOUUP\nBUEN DATO\n";

        InputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);

        FiltroDeLaPokedex.main(new String[]{});

        assertEquals("Pokemon from the HOENN region:\n" +
                     "REGISTEEL: STEEL - HOENN\n" +
                     "FLYING type Pokemon:\n" +
                     "ENAMORUS: FAIRY/FLYING - HISUI\n" +
                     "Pokemon from the GALAR region:\n" +
                     "FIRE type Pokemon:\n" +
                     "REGISTEEL entrydex: STEEL - HOENN\n" +
                     "STEEL type Pokemon:\n" +
                     "REGISTEEL: STEEL - HOENN\n" +
                     "Vayanse a Digimon, inutiles\n" +
                     "Gracias por usar la Pokedex. Tu total de búsquedas ha sido de 7, de las cuales 1 han sido inútiles por tu parte.\n",
                     getSystemOut());
    }
    @Test
    void testCasoDePrueba10() {
        String input = "3\nGOKU\nFIGHTING/PSYCHIC - KANTO\nNARUTO\nDARK/FIGHTING - HISUI\nCELESTEELA\nSTEEL/FLYING - ALOLA\nALOLA\nFLYING\nGOKU\nHISUI\nNARUTO\nCELESTEELA\nALOLAALOLA\nSTEEL\nNEVERGONNAGIVEYOUUP\nBUEN DATO\n";

        InputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);

        FiltroDeLaPokedex.main(new String[]{});

        assertEquals("Pokemon from the ALOLA region:\n" +
                     "CELESTEELA: STEEL/FLYING - ALOLA\n" +
                     "FLYING type Pokemon:\n" +
                     "CELESTEELA: STEEL/FLYING - ALOLA\n" +
                     "GOKU entrydex: FIGHTING/PSYCHIC - KANTO\n" +
                     "Pokemon from the HISUI region:\n" +
                     "NARUTO: DARK/FIGHTING - HISUI\n" +
                     "NARUTO entrydex: DARK/FIGHTING - HISUI\n" +
                     "CELESTEELA entrydex: STEEL/FLYING - ALOLA\n" +
                     "Vayanse a Digimon, inutiles\n" +
                     "STEEL type Pokemon:\n" +
                     "CELESTEELA: STEEL/FLYING - ALOLA\n" +
                     "Vayanse a Digimon, inutiles\n" +
                     "Gracias por usar la Pokedex. Tu total de búsquedas ha sido de 9, de las cuales 2 han sido inútiles por tu parte.\n",
                     getSystemOut());
    }
    @Test
    void testCasoDePrueba11() {
        String input = "0\nPIKACHU\nRAICHU\nMARC\nICE\nFIRE\nGALAR\nPETER PARKER\nBUEN DATO\n";

        InputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);

        FiltroDeLaPokedex.main(new String[]{});

        assertEquals("", getSystemOut());
    }
    @Test
    void testCasoDePrueba12() {
        String input = "3\nSCEPTILE\nGRASS - HOENN\nSCÉPTILE\nGRASS/DRAGON - HOENN\nDRAGONAIR\nDRAGON - KANTO\nHOENN\nScepTILE\nSCÉPTILE\nDRAGON\nDRAGONAIR\nNEVERGONNAGIVEYOUUP\nBUEN DATO\n";

        InputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);

        FiltroDeLaPokedex.main(new String[]{});

        assertEquals("Pokemon from the HOENN region:\n" +
                     "SCEPTILE: GRASS - HOENN\n" +
                     "SCÉPTILE: GRASS/DRAGON - HOENN\n" +
                     "Vayanse a Digimon, inutiles\n" +
                     "SCÉPTILE entrydex: GRASS/DRAGON - HOENN\n" +
                     "DRAGON type Pokemon:\n" +
                     "SCÉPTILE: GRASS/DRAGON - HOENN\n" +
                     "DRAGONAIR: DRAGON - KANTO\n" +
                     "DRAGONAIR entrydex: DRAGON - KANTO\n" +
                     "Vayanse a Digimon, inutiles\n" +
                     "Gracias por usar la Pokedex. Tu total de búsquedas ha sido de 6, de las cuales 2 han sido inútiles por tu parte.\n",
                     getSystemOut());
    }
    @Test
    void testCasoDePrueba13() {
        String input = "-3\nPIKACHU\nRAICHU\nMARC\nICE\nFIRE\nGALAR\nPETER PARKER\nBUEN DATO\n";

        InputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);

        FiltroDeLaPokedex.main(new String[]{});

        assertEquals("", getSystemOut());
    }
    @Test
    void testCasoDePrueba14() {
        String input = "3\nSCEPTILE\nGRASS - HOENN\nDRAGONITE\nGRASS/DRAGON - HOENN\nDRaGONITE\nDRAGON - KANTO\nHOENN\nScepTILE\nSCÉPTILE\nDRAGON\nDRAGONAIR\nNEVERGONNAGIVEYOUUP\nBUEN DATO\n";

        InputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);

        FiltroDeLaPokedex.main(new String[]{});

        assertEquals("Pokemon from the HOENN region:\n" +
                     "SCEPTILE: GRASS - HOENN\n" +
                     "DRAGONITE: GRASS/DRAGON - HOENN\n" +
                     "Vayanse a Digimon, inutiles\n" +
                     "Vayanse a Digimon, inutiles\n" +
                     "DRAGON type Pokemon:\n" +
                     "DRAGONITE: GRASS/DRAGON - HOENN\n" +
                     "DRaGONITE: DRAGON - KANTO\n" +
                     "Vayanse a Digimon, inutiles\n" +
                     "Vayanse a Digimon, inutiles\n" +
                     "Gracias por usar la Pokedex. Tu total de búsquedas ha sido de 6, de las cuales 4 han sido inútiles por tu parte.\n",
                     getSystemOut());
    }
    @Test
    void testCasoDePrueba15() {
        String input = "3\nCRESSELIA\nPSYCHIC - SINNOH\nSOLROCK\nROCK/PSYCHIC - HOENN\nPIKACHU\nELECTRIC - KANTO\nPSYCHIC\nHOENNUNOVAKALOSALOLA\nNEVERGONNAGIVEYOUUP\nICEFIREWATERGRASS\nCRESSELIASOLROCK\nBUEN DATO\n";

        InputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);

        FiltroDeLaPokedex.main(new String[]{});

        assertEquals("PSYCHIC type Pokemon:\n" +
                     "CRESSELIA: PSYCHIC - SINNOH\n" +
                     "SOLROCK: ROCK/PSYCHIC - HOENN\n" +
                     "Vayanse a Digimon, inutiles\n" +
                     "Vayanse a Digimon, inutiles\n" +
                     "Vayanse a Digimon, inutiles\n" +
                     "Vayanse a Digimon, inutiles\n" +
                     "Gracias por usar la Pokedex. Tu total de búsquedas ha sido de 5, de las cuales 4 han sido inútiles por tu parte.\n",
                     getSystemOut());
    }
    @Test
    void testCasoDePrueba16() {
        String input = "3\nREGIROCK\nROCK - HOENN\nSOLROCK\nROCK/PSYCHIC - HOENN\nREGIROCK2\nROCK/ROCK - HOENN\nROCK\nREGIROCK\nHOENN\nBUEN DATO\n";

        InputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);

        FiltroDeLaPokedex.main(new String[]{});

        assertEquals("ROCK type Pokemon:\n" +
                     "REGIROCK: ROCK - HOENN\n" +
                     "SOLROCK: ROCK/PSYCHIC - HOENN\n" +
                     "REGIROCK2: ROCK/ROCK - HOENN\n" +
                     "REGIROCK entrydex: ROCK - HOENN\n" +
                     "Pokemon from the HOENN region:\n" +
                     "REGIROCK: ROCK - HOENN\n" +
                     "SOLROCK: ROCK/PSYCHIC - HOENN\n" +
                     "REGIROCK2: ROCK/ROCK - HOENN\n" +
                     "Gracias por usar la Pokedex. Tu total de búsquedas ha sido de 3, de las cuales 0 han sido inútiles por tu parte.\n",
                     getSystemOut());
    }
    @Test
    void testCasoDePrueba17() {
        String input = "3\nROCKRUFF\nROCK - ALOLA\nPRIMARINA\nWATER/FAIRY - ALOLA\nNONEXISTENTPOKE\nROCK/HOENN - WATER\nROCK\nWATER\nROCKRUFF\nNONEXISTENTPOKE\nHOENN\nBUEN DATO\n";

        InputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);

        FiltroDeLaPokedex.main(new String[]{});

        assertEquals("ROCK type Pokemon:\n" +
                     "ROCKRUFF: ROCK - ALOLA\n" +
                     "NONEXISTENTPOKE: ROCK/HOENN - WATER\n" +
                     "WATER type Pokemon:\n" +
                     "PRIMARINA: WATER/FAIRY - ALOLA\n" +
                     "NONEXISTENTPOKE: ROCK/HOENN - WATER\n" +
                     "ROCKRUFF entrydex: ROCK - ALOLA\n" +
                     "NONEXISTENTPOKE entrydex: ROCK/HOENN - WATER\n" +
                     "Pokemon from the HOENN region:\n" +
                     "NONEXISTENTPOKE: ROCK/HOENN - WATER\n" +
                     "Gracias por usar la Pokedex. Tu total de búsquedas ha sido de 5, de las cuales 0 han sido inútiles por tu parte.\n",
                     getSystemOut());
    }

    private String getSystemOut() {
    	return null;
    }
} 
