package laPokedex;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Scanner;

public class pokehector {
	static int usos=-1;
	static int fallos=0;
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        LinkedHashMap<String, String> pokédex = new LinkedHashMap<>();
        
        int entradas = obtenerNumeroEntradas(sc);
        if (entradas > 0) {
        cargarPokedex(sc, pokédex, entradas);
        
        realizarBusquedas(sc, pokédex);
        
        
        sc.close();
    }
    }

    private static int obtenerNumeroEntradas(Scanner sc) {
        
        return sc.nextInt();
    }

    private static void cargarPokedex(Scanner sc, LinkedHashMap<String, String> pokédex, int entradas) {
        sc.nextLine(); // Para evitar el ataque de los que aparentan ser Pokémon de tipo Bicho
        for (int i = 0; i < entradas; i++) {
            String pokemon = sc.nextLine();
            String datosPokemon = sc.nextLine();
            pokédex.put(pokemon, datosPokemon);
        }
    }

    
    static void realizarBusquedas(Scanner sc, LinkedHashMap<String, String> pokédex) {
    	
        while (true) {
        	
            String busqueda = obtenerBusqueda(sc);
            usos++;

            if ("Buen dato".equalsIgnoreCase(busqueda)) {
                break;
            }
            
            realizarAccionBusqueda(pokédex, busqueda);
            
        }
        System.out.println("Gracias por usar la Pokedex. Tu total de busquedas ha sido de " + usos +
                ", de las cuales " + fallos + " han sido inutiles por tu parte.");
    }

    private static String obtenerBusqueda(Scanner sc) {
        return sc.nextLine();
    }

    private static void realizarAccionBusqueda(LinkedHashMap<String, String> pokédex, String busqueda) {
        if (pokédex.containsKey(busqueda)) {
            System.out.println(busqueda + " entrydex: " + pokédex.get(busqueda));
            
            
        } else if (esRegion(busqueda)) {
            imprimirPokemonPorRegion(pokédex, busqueda);
            
            
            
        } else if (esTipoPokemon(busqueda)) {
            imprimirPokemonPorTipo(pokédex, busqueda);
            
            
            
        } else {
        	fallos++;
            System.out.println("Vayanse a Digimon, inutiles");
            
            
        }
    }

    private static boolean esRegion(String busqueda) {
        List<String> regiones = Arrays.asList("KANTO", "JOHTO", "HOENN", "SINNOH", "UNOVA", "KALOS", "ALOLA", "GALAR", "HISUI", "PALDEA");
        return regiones.contains(busqueda);
    }

    private static void imprimirPokemonPorRegion(LinkedHashMap<String, String> pokédex, String region) {
        System.out.println("Pokemon de la region " + region + ":");
        pokédex.forEach((pokemon, datos) -> {
            if (datos.contains(region)) {
                System.out.println(pokemon + ": " + datos);
            }
        });
    }

    private static boolean esTipoPokemon(String busqueda) {
        List<String> tipos = Arrays.asList("BUG", "DARK", "DRAGON", "ELECTRIC", "FAIRY", "FIGHTING", "FIRE", "FLYING", "GHOST", "GRASS", "GROUND", "ICE", "NORMAL", "POISON", "PSYCHIC", "ROCK", "STEEL", "WATER");
        return tipos.contains(busqueda);
    }

    private static void imprimirPokemonPorTipo(LinkedHashMap<String, String> pokédex, String tipo) {
        System.out.println("Pokemon tipo " + tipo + ":");
        pokédex.forEach((pokemon, datos) -> {
            if (datos.contains(tipo)) {
                System.out.println(pokemon + ": " + datos);
            }
        });
    
    }
}
